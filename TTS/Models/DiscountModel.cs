﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TTS.Controllers;

namespace TTS.Models
{
    public class DiscountModel
    {
        public string id { get; set; }
        public string name { get; set; }

        [Display(Name = "ลำดับ")]
        public int number { get; set; }
        public string DisCode { get; set; }
        [Display(Name = "ประเภทบัตร/คูปอง")]
        public string cardType { get; set; }
        public IEnumerable<SelectListItem> CardType { get; set; }
        [Display(Name = "ส่วนลด(%)")]
        public decimal discountPer { get; set; }
        [Display(Name = "ส่วนลด(บาท)")]
        public decimal discount { get; set; }


    }

    public class Discount
    {
        public IEnumerable<DiscountModel> getDiscountList(string strProductCode)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCountsCom(function.EmptyIfNull(""), function.EmptyIfNull(""));
            }
            return new List<DiscountModel>();
        }

        public IEnumerable<DiscountModel> getCouponList()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCounts();
            }

            List<DiscountModel> ListCouponList = dt.AsEnumerable().Select(m => new DiscountModel()
            {
                id = m.Field<string>("DisCode").ToString(),
                name = m.Field<string>("DisName").ToString(),
                DisCode = m.Field<string>("DisCode").ToString(),
                cardType = m.Field<string>("DisName").ToString(),
                discountPer = m.Field<decimal>("DisPer"),
                discount = m.Field<decimal>("DisAm")
            })
            .ToList();
            return ListCouponList;
        }

        public IEnumerable<DiscountModel> getCouponList(string strCode)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCounts();
            }

            DataTable dtNew = dt.Select("DisCode='" + strCode + "'").CopyToDataTable();

            List<DiscountModel> ListCouponList = dtNew.AsEnumerable().Select(m => new DiscountModel()
            {
                id = m.Field<string>("DisCode").ToString(),
                name = m.Field<string>("DisName").ToString(),
                DisCode = m.Field<string>("DisCode").ToString(),
                cardType = m.Field<string>("DisName").ToString(),
                discountPer = m.Field<decimal>("DisPer"),
                discount = m.Field<decimal>("DisAm")
            })
            .ToList();
            return ListCouponList;
        }

        public IEnumerable<DiscountModel> getCouponExtraList()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCountExtra();
            }

            List<DiscountModel> ListCouponList = dt.AsEnumerable().Select(m => new DiscountModel()
            {
                id = m.Field<string>("Code").ToString(),
                name = m.Field<string>("Description").ToString(),
                DisCode = m.Field<string>("Code").ToString(),
                cardType = m.Field<string>("Description").ToString(),
                discountPer = m.Field<decimal>("Percent"),
                discount = m.Field<decimal>("Baht")
            })
            .ToList();
            return ListCouponList;
        }

        public IEnumerable<DiscountModel> getCouponExtraList(string strCode)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCountExtra();
            }

            DataTable dtNew = dt.Select("Code='" + strCode + "'").CopyToDataTable();

            List<DiscountModel> ListCouponList = dtNew.AsEnumerable().Select(m => new DiscountModel()
            {
                id = m.Field<string>("Code").ToString(),
                name = m.Field<string>("Description").ToString(),
                DisCode = m.Field<string>("Code").ToString(),
                cardType = m.Field<string>("Description").ToString(),
                discountPer = m.Field<decimal>("Percent"),
                discount = m.Field<decimal>("Baht")
            })
            .ToList();
            return ListCouponList;
        }

        public IEnumerable<DiscountModel> getDiscountPerList(string strPGSC, string strDiscountID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCountsCom(strPGSC, strDiscountID);
            }

            List<DiscountModel> ListCouponList = dt.AsEnumerable().Select(m => new DiscountModel()
            {
                discountPer = m.Field<decimal>("DeptDiscount")
            })
            .ToList();
            return ListCouponList;
        }

    }

}
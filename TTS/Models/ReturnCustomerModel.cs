﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using TTS.Controllers;

namespace TTS.Models
{
    public class ReturnCustomerModel
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public int LineNumber { get; set; }
        public string InvoiceNumber { get; set; }
        [Display(Name = "รหัสสินค้า")]
        public string PGSC { get; set; }
        [Display(Name = "ขนาด")]
        public string Size { get; set; }
        [Display(Name = "ปริมาณ")]
        public int Qty { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string IsExport { get; set; }
        public string RefNumber { get; set; }
        public string ItemStatus { get; set; }
        [Display(Name = "ส่วนลดบริษัท")]
        public decimal DiscountPer { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountOnTop { get; set; }
        public decimal DiscountHdPer { get; set; }
        public decimal DiscountHdAm { get; set; }
        public string GPCode { get; set; }
        public decimal GP { get; set; }
        public int QtyOrder { get; set; }
        [Display(Name = "ราคาป้าย")]
        public decimal Price { get; set; }
        [Display(Name = "ราคาขาย")]
        public decimal PriceTotal { get; set; }
        public decimal Vat { get; set; }
        public decimal LineTotal { get; set; }
        public string CustGroupCode { get; set; }
        public string CompanyCode { get; set; }
        public System.DateTime DocDate { get; set; }
        [Display(Name = "รหัสสาขา")]
        public string BranchCode { get; set; }
        public Nullable<int> IdSales { get; set; }
        public string WHCode { get; set; }
        [Display(Name = "ชื่อสินค้า")]
        public string Style { get; set; }
    }
    public class ReturnCustomer
    {
        public IEnumerable<ReturnCustomerModel> getReturnCustomer(string OrderNumber, string BranchCode, string ProductId, string SizeName, string OrderDate, string ToOrderDate, string IsExport)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);

            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;
            DataSet ds = new DataSet();

            //var myDate = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }


            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectReturnCustomerDt(function.EmptyIfNull(BranchCode), function.EmptyIfNull(OrderNumber), function.EmptyIfNull(ProductId), function.EmptyIfNull(SizeName), dateStart, dateStop, function.EmptyIfNull(IsExport));

            }

            //dt = cSQLExcute.ExecuteDataAdapter("SELECT * FROM v_SalesOrderDetail", CommandType.Text).Tables[0];


            List<ReturnCustomerModel> listSalesOrder = dt.AsEnumerable().Select(m => new ReturnCustomerModel()
            {
                //Display in Grid
                Id = m.Field<int>("Id"),
                OrderNumber = m.Field<string>("OrderNumber"),
                DocDate = m.Field<DateTime>("DocDate"),
                BranchCode = m.Field<string>("BranchCode"),
                LineNumber = m.Field<int>("LineNumber"),
                InvoiceNumber = m.Field<string>("InvoiceNumber"),
                PGSC = m.Field<string>("PGSC"), //ProductId
                //ProductName = m.Field<string>("ProductName"),

                PriceTotal = m.Field<decimal>("PriceTotal"),
                DiscountOnTop = m.Field<decimal>("DiscountOnTop"),
                DiscountPer = m.Field<decimal>("DiscountPer"),
                Discount = m.Field<decimal>("Discount"),
                Qty = m.Field<int>("Qty"),
                GP = m.Field<decimal>("GP"),

                LineTotal = m.Field<decimal>("LineTotal"),
                Vat = m.Field<decimal>("Vat"),
                Size = m.Field<string>("Size"),
                IsExport = m.Field<string>("IsExport"),
                GPCode = m.Field<string>("GPCode")
            }).ToList();

            return listSalesOrder;
        }

        public List<BranchFromSalesOrderModel> getBranchFromReturnCustomer(string custGroup, string BranchCode, string OrderDate, string ToOrderDate)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectBranchCodeToSalesOrder(function.EmptyIfNull(custGroup), function.EmptyIfNull(BranchCode), dateStart, dateStop);
            }

            List<BranchFromSalesOrderModel> listSalesOrder = dt.AsEnumerable().Select(m => new BranchFromSalesOrderModel()
            {
                //Display in Grid
                CustGroupCode = m.Field<string>("CustGroupCode"),
                Description = m.Field<string>("Description"),
                BranchCode = m.Field<string>("BranchCode"),
                CustNameThai = m.Field<string>("CustNameThai"),
                RecordsPerGroup = m.Field<int>("RecordsPerGroup"),
                TotalRecords = m.Field<int>("TotalRecords")
            }).ToList();

            return listSalesOrder;
        }

        public DataTable getReturnCustomerDetailTable()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectReturnCustomerDetailDt("");
            }
            if (dt.Rows.Count > 0)
                dt.Rows.Clear();
            return dt;
        }

        public ReturnCustomerModel getReturnCustomerDetail(string Id)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);

            DataTable dt = new DataTable();

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectReturnCustomerDetailDt(Id);

            }

            ReturnCustomerModel s = new ReturnCustomerModel();

            //Display in Grid
            s.Id = (int)dt.Rows[0]["Id"];
            s.OrderNumber = dt.Rows[0]["OrderNumber"].ToString();
            s.DocDate = Convert.ToDateTime(dt.Rows[0]["DocDate"]);
            s.BranchCode = dt.Rows[0]["BranchCode"].ToString();
            s.LineNumber = (int)dt.Rows[0]["LineNumber"];
            s.InvoiceNumber = dt.Rows[0]["InvoiceNumber"].ToString();
            s.PGSC = dt.Rows[0]["PGSC"].ToString(); //ProductId
            //ProductName = m.Field<string>("ProductName"),
            s.Price = (decimal)dt.Rows[0]["Price"];
            s.PriceTotal = (decimal)dt.Rows[0]["PriceTotal"];
            s.DiscountOnTop = (decimal)dt.Rows[0]["DiscountOnTop"];
            s.DiscountPer = (decimal)dt.Rows[0]["DiscountPer"];
            s.Discount = (decimal)dt.Rows[0]["Discount"];
            s.DiscountHdPer = (decimal)dt.Rows[0]["DiscountHdPer"];
            s.DiscountHdAm = (decimal)dt.Rows[0]["DiscountHdAm"];
            s.Qty = (int)dt.Rows[0]["Qty"];
            s.GP = (decimal)dt.Rows[0]["GP"];

            s.LineTotal = (decimal)dt.Rows[0]["LineTotal"];
            s.Vat = (decimal)dt.Rows[0]["Vat"];
            s.Size = dt.Rows[0]["Size"].ToString();
            s.IsExport = dt.Rows[0]["IsExport"].ToString();
            s.GPCode = dt.Rows[0]["GPCode"].ToString();
            s.Style = dt.Rows[0]["Style"].ToString();

            s.WHCode = dt.Rows[0]["WHCode"].ToString();
            s.CompanyCode = dt.Rows[0]["CompanyCode"].ToString();
            s.BranchCode = dt.Rows[0]["BranchCode"].ToString();
            s.RefNumber = dt.Rows[0]["RefNumber"].ToString();
            return s;
        }

        public string updateReturnCustomer(DataRow drItem)
        {
            ResultEntity result = new ResultEntity();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                result = dal.UpdateReturnCustomer(drItem, HttpContext.Current.Session["ses_user_id"].ToString());
            }
            return result.MassageText;
        }

        public bool deleteReturnCustomer(int id)
        {
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dal.DeleteReturnCustomer(id, HttpContext.Current.Session["ses_user_id"].ToString());
            }
            return true;
        }

        public List<string> SendReturnCustomerToHost(DataTable dt)
        {
            List<string> returnValue = new List<string>();
            ResultEntity result = new ResultEntity();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                result = dal.SendReturnCustomerToHost(dt);
            }

            if (result.ResultCode == 0)
            {
                returnValue.Add("");
                returnValue.Add("ส่งข้อมูลเรียบร้อยแล้ว!");
            }
            else
            {
                returnValue.Add("Error!");
                returnValue.Add(result.MassageText);
            }
            return returnValue;
        }
    }
}
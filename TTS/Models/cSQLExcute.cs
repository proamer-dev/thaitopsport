﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace TTS.Models
{

    public class cSQLExcute
    {

        public static Boolean TestConnectDB(string strConnection)
        {
            try
            {
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

       private static string strConnection = HttpContext.Current.Session["ses_sys_db_connection"].ToString();

        public static Boolean ExecuteCommand(string strSQL, CommandType strCommandType)
        {
            try
            {
                SqlTransaction myTransaction;
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    myTransaction = myConnection.BeginTransaction();
                    using (SqlCommand myCommand = new SqlCommand(strSQL, myConnection))
                    {
                        myCommand.Transaction = myTransaction;
                        myCommand.CommandType = strCommandType;
                        try
                        {
                            myCommand.ExecuteNonQuery();
                            myTransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            myTransaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Boolean ExecuteCommand(string strSQL,
                                             CommandType strCommandType,
                                             SqlParameter[] Parameter)
        {
            try
            {
                SqlTransaction myTransaction;
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    myTransaction = myConnection.BeginTransaction();
                    using (SqlCommand myCommand = new SqlCommand(strSQL, myConnection))
                    {
                        myCommand.Transaction = myTransaction;
                        myCommand.CommandType = strCommandType;
                        myCommand.Parameters.AddRange(Parameter);
                        try
                        {
                            myCommand.ExecuteNonQuery();
                            myTransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            myTransaction.Rollback();
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static Boolean ExecuteCommand(IEnumerable<object> obj,
        //                                     CommandType strCommandType,
        //                                     SqlParameter[] Parameter)
        //{
        //    try
        //    {
        //        SqlTransaction myTransaction;
        //        using (SqlConnection myConnection = new SqlConnection(strConnection))
        //        {
        //            myConnection.Open();
        //            myTransaction = myConnection.BeginTransaction();

        //            foreach (object s in obj)
        //            {
        //                using (SqlCommand myCommand = new SqlCommand("usp_update_userAuthentication", myConnection))
        //                {
        //                    myCommand.Transaction = myTransaction;
        //                    myCommand.CommandType = CommandType.StoredProcedure;
        //                    myCommand.Parameters.AddWithValue("@app_id", Controllers.SiteController.AppID);
        //                    myCommand.Parameters.AddWithValue("@user_group_id", s.menu_id);
        //                    myCommand.Parameters.AddWithValue("@menu_id", s.menu_id);
        //                    myCommand.Parameters.AddWithValue("@is_active", (s.str_is_active == "yes" ? "YES" : "NO"));
        //                    myCommand.Parameters.AddWithValue("@create_by", HttpContext.Current.Session["ses_user_id"].ToString());
        //                    myCommand.ExecuteNonQuery();
        //                }
        //            }

        //            try
        //            {
        //                myTransaction.Commit();
        //                return true;
        //            }
        //            catch (Exception ex)
        //            {
        //                myTransaction.Rollback();
        //                throw ex;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static DataSet ExecuteDataAdapter(string sqlSQL,
                                                 CommandType strCommandType)
        {
            try
            {
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    using (SqlDataAdapter myDataAdapter = new SqlDataAdapter(sqlSQL, myConnection))
                    {
                        DataSet ds = new DataSet();
                        myDataAdapter.SelectCommand.CommandTimeout = 1800;
                        myDataAdapter.SelectCommand.CommandType = strCommandType;
                        myDataAdapter.Fill(ds);
                        return ds;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet ExecuteDataAdapter(string sqlSQL,
                                                 CommandType strCommandType,
                                                 SqlParameter[] Parameter)
        {

            try
            {
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    using (SqlDataAdapter myDataAdapter = new SqlDataAdapter(sqlSQL, myConnection))
                    {
                        DataSet ds = new DataSet();
                        myDataAdapter.SelectCommand.CommandTimeout = 1800;
                        myDataAdapter.SelectCommand.CommandType = strCommandType;
                        myDataAdapter.SelectCommand.Parameters.AddRange(Parameter);
                        myDataAdapter.Fill(ds);
                        return ds;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object ExecuteScalar(string strSQL,
                                           CommandType strCommandType)
        {
            try
            {
                //Controllers.SiteController sc = new Controllers.SiteController();
                //string strConnection = sc.strConnection;
                object objReturnValue;
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    using (SqlCommand myCommand = new SqlCommand(strSQL, myConnection))
                    {
                        myCommand.CommandType = strCommandType;
                        return objReturnValue = myCommand.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object ExecuteScalar(string strSQL,
                                           CommandType strCommandType,
                                           SqlParameter[] Parameter)
        {
            try
            {
                //Controllers.SiteController sc = new Controllers.SiteController();
                //string strConnection = sc.strConnection;
                object objReturnValue;
                using (SqlConnection myConnection = new SqlConnection(strConnection))
                {
                    myConnection.Open();
                    using (SqlCommand myCommand = new SqlCommand(strSQL, myConnection))
                    {
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.Parameters.AddRange(Parameter);
                        return objReturnValue = myCommand.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
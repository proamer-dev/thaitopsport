﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DataLibrary;
using System.Web.Mvc;
using TTS.Controllers;

namespace TTS.Models
{
    public class BranchModel
    {
        public string BranchId { get; set; }
        public string BranchName { get; set; }
        public IEnumerable<SelectListItem> Branch { get; set; }

        public string CustGroupCode { get; set; }
        public string Description { get; set; } //Cust Group Name
    }

    public class Branch
    {
        public IEnumerable<BranchModel> getBranch(string BranchCode = null)
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new WarehouseDAL())
            {
                dt = dal.GetBranchCodeAll();
            }

            List<BranchModel> listBranch = dt.AsEnumerable().Select(m => new BranchModel()
            {
                BranchId = m.Field<string>("OrderNumber"),
                BranchName = m.Field<string>("OrderNumber")
            }).ToList();
            return listBranch;
        }

        public List<SelectListItem> getBranchAll()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new WarehouseDAL())
            {
                dt = dal.GetBranchCodeAll();
            }
            List<SelectListItem> ListBranch = dt.AsEnumerable().Select(m => new SelectListItem()
            {
                Value = m.Field<string>("BranchCode"),
                Text = m.Field<string>("BranchCode")
            }).ToList();

            return ListBranch;
        }

        public List<BranchModel> getBranchList()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new WarehouseDAL())
            {
                dt = dal.GetBranchCodeAll();
            }
            //var sector1Results = (from r in dt.AsEnumerable()
            //select r["BranchCode"]).Distinct().ToList(); ;
            dt.DefaultView.Sort = "BranchCode ASC";
            var sector1Results = dt.DefaultView.ToTable(true, "BranchCode");

            List<BranchModel> ListBranch = sector1Results.AsEnumerable().Select(m => new BranchModel()
            {
                BranchId = m.Field<string>("BranchCode"),
                BranchName = m.Field<string>("BranchCode")
            }).ToList();

            return ListBranch;
        }

        public List<BranchModel> getBranchList(string CustGroupCode)
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new WarehouseDAL())
            {
                dt = dal.GetBranchCodeAll(HttpContext.Current.Session["ses_user_id"].ToString(), function.NullIfEmpty(CustGroupCode));
            }

            dt.DefaultView.Sort = "CustNameThai ASC";
            //var sector1Results = dt.DefaultView.ToTable(true, "BranchCode");

            List<BranchModel> ListBranch = dt.AsEnumerable().Select(m => new BranchModel()
            {
                BranchId = m.Field<string>("CustCode"),
                BranchName = "(" + m.Field<string>("CustCode") + ") " + m.Field<string>("CustNameThai")
            }).ToList();

            return ListBranch;
        }

        public List<BranchModel> getCustGroup()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new WarehouseDAL())
            {
                dt = dal.GetCustCodeAll(HttpContext.Current.Session["ses_user_id"].ToString());
            }
            //var sector1Results = (from r in dt.AsEnumerable()
            //select r["BranchCode"]).Distinct().ToList(); ;
            dt.DefaultView.Sort = "Description ASC";
            var sector1Results = dt.DefaultView.ToTable(true, "CustGroupCode", "Description");

            List<BranchModel> ListCustGroup = sector1Results.AsEnumerable().Select(m => new BranchModel()
            {
                CustGroupCode = m.Field<string>("CustGroupCode"),
                Description = m.Field<string>("Description")
            }).ToList();

            return ListCustGroup;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TTS.Models
{
    public class UserGroupModel
    {
        [Required]
        [Display(Name = "Group ID")]
        public string user_group_id { get; set; }

        public string app_id { get; set; }

        [Required]
        [Display(Name = "Group name")]
        [ReadOnly(true)]
        public string name { get; set; }

        [Display(Name = "Description")]
        public string description { get; set; }

        [Display(Name = "Active")]
        public string is_active { get; set; }

        public DateTime create_date { get; set; }
        public string create_by { get; set; }
        public byte[] rowversion { get; set; }
    }

    public class UserGroup
    {
        public UserGroupModel getUserGroup_groupID(string user_group_id)
        {
            try
            {
                SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                            new SqlParameter("@user_group_id", user_group_id)
                        };
                DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_get_userGroup", CommandType.StoredProcedure, Parameter).Tables[0];

                UserGroupModel ug = new UserGroupModel();
                ug.user_group_id = dt.Rows[0]["user_group_id"].ToString();
                ug.app_id = dt.Rows[0]["app_id"].ToString();
                ug.name = dt.Rows[0]["name"].ToString();
                ug.description = dt.Rows[0]["description"].ToString();
                ug.is_active = dt.Rows[0]["is_active"].ToString();
                ug.create_date = Convert.ToDateTime(dt.Rows[0]["create_date"]);

                return ug;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<UserGroupModel> getUserGroup(string group_name, string group_desc)
        {
            try
            {
                Controllers.Site s = new Controllers.Site();
                SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@name", Controllers.function.NullIfEmpty(group_name)),
                             new SqlParameter("@description", Controllers.function.NullIfEmpty(group_desc))
                        };
                DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_get_userGroup", CommandType.StoredProcedure, Parameter).Tables[0];

                List<UserGroupModel> listUser = dt.AsEnumerable().Select(m => new UserGroupModel()
                {
                    user_group_id = m.Field<string>("user_group_id"),
                    app_id = m.Field<string>("app_id"),
                    name = m.Field<string>("name"),
                    description = m.Field<string>("description"),
                    is_active = m.Field<string>("is_active"),
                    create_date = m.Field<DateTime>("create_date")
                }).ToList();

                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getUserGroup_dt(string group_name, string group_desc)
        {
            try
            {
                Controllers.Site s = new Controllers.Site();
                SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@name", Controllers.function.NullIfEmpty(group_name)),
                             new SqlParameter("@description", Controllers.function.NullIfEmpty(group_desc))
                        };
                DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_get_userGroup", CommandType.StoredProcedure, Parameter).Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SecurityService.ResultEntity updateUserGroup(string user_group_id, string name, string description)
        {
            Controllers.Site s = new Controllers.Site();
            SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@user_group_id", Controllers.function.NullIfEmpty(user_group_id)),
                             new SqlParameter("@name", Controllers.function.NullIfEmpty(name)),
                             new SqlParameter("@description", Controllers.function.NullIfEmpty(description)),
                             new SqlParameter("@create_by", HttpContext.Current.Session["ses_user_id"].ToString())
                        };
            DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_update_userGroup", CommandType.StoredProcedure, Parameter).Tables[0];

            SecurityService.ResultEntity result = new SecurityService.ResultEntity();
            result.ResultCode = Convert.ToInt32(dt.Rows[0]["ResultCode"].ToString());
            return result;
        }

        public SecurityService.ResultEntity deleteUserGroup(string user_group_id)
        {
            Controllers.Site s = new Controllers.Site();
            SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@user_group_id", Controllers.function.NullIfEmpty(user_group_id))
                        };
            DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_delete_userGroup", CommandType.StoredProcedure, Parameter).Tables[0];

            SecurityService.ResultEntity result = new SecurityService.ResultEntity();
            result.ResultCode = Convert.ToInt32(dt.Rows[0]["ResultCode"].ToString());
            return result;
        }

    }
}
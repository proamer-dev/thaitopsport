﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTS.Models
{
    public class GPModel
    {
        public string gp { get; set; }
        public IEnumerable<SelectListItem> GP { get; set; }
    }

    public class GP
    {
        public IEnumerable<SelectListItem> getGP(string strBranch)
        {
            try
            {
                DataTable dt = new DataTable();
                GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
                using (var dal = new SalesOrderDAL())
                {
                    var result = dal.GetDisCountsCom(strBranch);
                }

                List<SelectListItem> ListSize = dt.AsEnumerable().Select(m => new SelectListItem()
                {
                    Value = m.Field<string>("CodeDisCount").ToString(),
                    Text = m.Field<string>("CodeDisCount")
                }).ToList();
                return ListSize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<GPModel> getGPList(string strBranch)
        {
            try
            {
                DataTable dt = new DataTable();
                GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
                using (var dal = new SalesOrderDAL())
                {
                    dt = dal.GetDisCountsCom(strBranch);
                }

                List<GPModel> ListGP = dt.AsEnumerable().Select(m => new GPModel()
                {
                    gp = m.Field<string>("CodeDisCount").ToString()
                }).ToList();
                return ListGP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
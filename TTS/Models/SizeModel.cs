﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTS.Models
{
    public class SizeModel
    {
        public string size_id { get; set; }
        public string name { get; set; }
        public IEnumerable<SelectListItem> Size { get; set; }
    }

    public class Size
    {
        public IEnumerable<SelectListItem> getSize()
        {
            try
            {
                DataTable dt = new DataTable();
                GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
                using (var dal = new ProductsDAL())
                {
                    dt = dal.GetSize();
                }

                //List<SelectListItem> ListSize = dt.AsEnumerable().Select(m => new SelectListItem()
                //                {
                //                    Value = m.Field<int>("Id").ToString(),
                //                    Text = m.Field<string>("SizeName")
                //                }).ToList();
                List<SelectListItem> ListSize = dt.AsEnumerable().Select(m => new SelectListItem()
                {
                    Value = m.Field<int>("Id").ToString(),
                    Text = m.Field<string>("SizeName")
                }).ToList();
                return ListSize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SizeModel> getSizeList()
        {
            try
            {
                DataTable dt = new DataTable();
                GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
                using (var dal = new ProductsDAL())
                {
                    dt = dal.GetSize();
                }
                List<SizeModel> ListSize = dt.AsEnumerable().Select(m => new SizeModel()
                {
                    size_id = m.Field<int>("Id").ToString(),
                    name = m.Field<string>("SizeName")
                }).ToList();
                return ListSize;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
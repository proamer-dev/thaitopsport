﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TTS.Models
{
    public class AuthorizeChildMenuModel
    {
        public string menu_id { get; set; }
        public string menu_name { get; set; }
        public int menu_group_sequence { get; set; }
        public int menu_sequence { get; set; }
        public bool is_active { get; set; }

    }

    public class AuthorizeParentMenuModel
    {
        public string menu_group_id { get; set; }
        public string menu_id { get; set; }
        public string menu_name { get; set; }
        public int menu_group_sequence { get; set; }
        public int menu_sequence { get; set; }
        public bool is_active { get; set; }
        public string str_is_active { get; set; }

        public IList<AuthorizeChildMenuModel> AuthorizeChildMenuModel { get; set; }

        public AuthorizeParentMenuModel()
        {
            AuthorizeChildMenuModel = new List<AuthorizeChildMenuModel>();
        }
    }

    public class Authorize
    {
        public IEnumerable<AuthorizeParentMenuModel> getUserGroupMenu(string strUserGroup)
        {
            try
            {
                Controllers.Site s = new Controllers.Site();
                SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@user_group_id", Controllers.function.NullIfEmpty(strUserGroup))
                        };
                DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_get_user_group_menu", CommandType.StoredProcedure, Parameter).Tables[0];

                //List<AuthorizeParentMenuModel> listUser = dt.AsEnumerable().Select(m => new AuthorizeParentMenuModel()
                //{
                //    menu_id = m.Field<string>("menu_id"),
                //    menu_name = m.Field<string>("menu_name"),
                //    menu_group_sequence = m.Field<int>("menu_group_sequence"),
                //    menu_sequence = m.Field<int>("menu_sequence"),
                //    is_active = m.Field<bool>("is_active")
                //}).ToList();

                List<AuthorizeParentMenuModel> model = new List<AuthorizeParentMenuModel>();
                foreach (DataRow row in dt.Rows)
                {
                    if (row["parent_menu_id"].ToString() == "")
                    {
                        //เพิ่มเมนูย่อย
                        var AuthorizeChildMenu = new List<AuthorizeChildMenuModel>();
                        var rows = dt.Select("[parent_menu_id] = '" + row["menu_id"].ToString() + "'");
                        var dtRow = rows.Any() ? rows.CopyToDataTable() : null;
                        if (dtRow != null && dtRow.Rows.Count > 0)
                        {
                            foreach (DataRow rowChild in dtRow.Rows)
                            {
                                AuthorizeChildMenuModel ChildMenu = new AuthorizeChildMenuModel();
                                ChildMenu.menu_id = rowChild["menu_id"].ToString();
                                ChildMenu.menu_name = rowChild["menu_name"].ToString();
                                ChildMenu.is_active = Convert.ToBoolean((rowChild["is_active"].ToString() == "YES" ? 1 : 0));
                                AuthorizeChildMenu.Add(ChildMenu);
                            }
                        }

                        AuthorizeParentMenuModel ParentMenu = new AuthorizeParentMenuModel
                        {
                            menu_id = row["menu_id"].ToString(),
                            menu_name = row["menu_name"].ToString(),
                            is_active = Convert.ToBoolean((row["is_active"].ToString() == "YES" ? 1 : 0)),
                            AuthorizeChildMenuModel = AuthorizeChildMenu
                        };
                        model.Add(ParentMenu);
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public SecurityService.ResultEntity updateUserGroupMenu(List<Models.AuthorizeParentMenuModel> lsMenu)
        {
            SecurityService.ResultEntity result = new SecurityService.ResultEntity();
            try
            {
                SqlTransaction myTransaction;
                using (SqlConnection myConnection = new SqlConnection(HttpContext.Current.Session["ses_sys_db_connection"].ToString()))
                {
                    myConnection.Open();
                    myTransaction = myConnection.BeginTransaction();

                    Controllers.Site site = new Controllers.Site();
                    foreach (AuthorizeParentMenuModel s in lsMenu)
                    {
                        using (SqlCommand myCommand = new SqlCommand("usp_update_userAuthentication", myConnection))
                        {
                            myCommand.Transaction = myTransaction;
                            myCommand.CommandType = CommandType.StoredProcedure;
                            myCommand.Parameters.AddWithValue("@app_id", site.AppID);
                            myCommand.Parameters.AddWithValue("@user_group_id", s.menu_group_id);
                            myCommand.Parameters.AddWithValue("@menu_id", s.menu_id);
                            myCommand.Parameters.AddWithValue("@is_active", (s.str_is_active == "true" ? "YES" : "NO"));
                            myCommand.Parameters.AddWithValue("@create_by", HttpContext.Current.Session["ses_user_id"].ToString());
                            myCommand.ExecuteNonQuery();
                        }
                    }

                    try
                    {
                        myTransaction.Commit();
                        result.ResultCode = 0;
                        return result;
                    }
                    catch (Exception ex)
                    {
                        myTransaction.Rollback();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                result.ResultCode = 1;
                result.MessageText = ex.Message;
                return result;
            }
        }
    }
}
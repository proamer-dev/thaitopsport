﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using TTS.Controllers;

namespace TTS.Models
{
    public class ReturnCompanyModel
    {
        public int Id { get; set; }
        public string BranchCode { get; set; }
        [Display(Name = "เลขที่งาน")]
        public string OrderNumber { get; set; }
        [Display(Name = "รหัสสินค้า")]
        public string ProductId { get; set; }
        [Display(Name = "ชื่อสินค้า")]
        public string ProductName { get; set; }
        [Display(Name = "ขนาด")]
        public string Size { get; set; }
        [Display(Name = "ปริมาณ")]
        public int Qty { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string IsExport { get; set; }
        public string RefNumber { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        [Display(Name = "คลังปลายทาง")]
        public string WHCode { get; set; }
        [Display(Name = "ผู้สั่งโอน")]
        public string ApproveBy { get; set; }
        [Display(Name = "เหตุผล")]
        public string Reason { get; set; }
        public List<ReturnCompanyModel> ReturnCompany { get; set; }
        public string InvoiceNumber { get; set; }
        [Display(Name = "ราคาป้าย")]
        public decimal RTPrice { get; set; }
        public int LineNumber { get; set; }
    }

    public class ReturnCompany
    {
        public IEnumerable<ReturnCompanyModel> getReturnCompany(string OrderNumber, string BranchCode, string ProductId, string SizeName, string OrderDate, string ToOrderDate, string IsExport)
        {
            

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                dt = dal.SelectOrderDetail(function.EmptyIfNull(OrderNumber), function.EmptyIfNull(BranchCode), function.EmptyIfNull(ProductId), function.EmptyIfNull(SizeName), dateStart, dateStop, function.EmptyIfNull(IsExport));
            }

            List<ReturnCompanyModel> ReturnCompany = dt.AsEnumerable().Select(m => new ReturnCompanyModel()
            {
                //Display in Grid
                Id = m.Field<int>("Id"),
                OrderNumber = m.Field<string>("OrderNumber"),
                OrderDate = m.Field<DateTime>("OrderDate"),
                BranchCode = m.Field<string>("BranchCode"),
                ProductId = m.Field<string>("ProductId"),

                Size = m.Field<string>("Size"),
                Qty = m.Field<int>("Qty"),
                WHCode = m.Field<string>("WHCode"),
                ApproveBy = m.Field<string>("ApproveBy"),
                Reason = m.Field<string>("Reason"),
                IsExport = m.Field<string>("IsExport"),
                RefNumber = m.Field<string>("RefNumber"),
                LineNumber = m.Field<int>("LineNumber")
            }).ToList();

            return ReturnCompany;
        }



        public List<BranchFromSalesOrderModel> getBranchFromReturnCompany(string custGroup, string BranchCode, string OrderDate, string ToOrderDate)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                dt = dal.SelectBranchCodeToReturnCompany(function.EmptyIfNull(custGroup), function.EmptyIfNull(BranchCode), dateStart, dateStop);
            }

            List<BranchFromSalesOrderModel> listSalesOrder = dt.AsEnumerable().Select(m => new BranchFromSalesOrderModel()
            {
                //Display in Grid
                CustGroupCode = m.Field<string>("CustGroupCode"),
                Description = m.Field<string>("Description"),
                BranchCode = m.Field<string>("BranchCode"),
                CustNameThai = m.Field<string>("CustNameThai"),
                RecordsPerGroup = m.Field<int>("RecordsPerGroup"),
                TotalRecords = m.Field<int>("TotalRecords")
            }).ToList();

            return listSalesOrder;
        }

        public ReturnCompanyModel getReturnCompanyDetail(string id)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                dt = dal.SelectOrderDetail(id);
            }

            if (dt.Rows.Count > 0)
            {
                ReturnCompanyModel s = new ReturnCompanyModel();
                s.Id = (int)dt.Rows[0]["Id"];
                s.OrderNumber = dt.Rows[0]["OrderNumber"].ToString();
                s.OrderDate = Convert.ToDateTime(dt.Rows[0]["OrderDate"]);
                s.BranchCode = dt.Rows[0]["BranchCode"].ToString();
                s.ProductId = dt.Rows[0]["ProductId"].ToString();
                s.ProductName = dt.Rows[0]["Style"].ToString();
                s.Size = dt.Rows[0]["Size"].ToString();
                s.Qty = (int)dt.Rows[0]["Qty"];
                s.WHCode = dt.Rows[0]["WHCode"].ToString();
                s.ApproveBy = dt.Rows[0]["ApproveBy"].ToString();
                s.Reason = dt.Rows[0]["Reason"].ToString();
                s.IsExport = dt.Rows[0]["IsExport"].ToString();
                s.RefNumber = dt.Rows[0]["RefNumber"].ToString();
                s.InvoiceNumber = dt.Rows[0]["InvoiceNumber"].ToString();
                s.RTPrice = (decimal)dt.Rows[0]["RTPrice"];
                return s;
            }
            else
                return null;
        }

        public DataTable getReturnCompanyTable()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                dt = dal.SelectOrderDetail("");
            }

            if (dt.Rows.Count > 0)
                dt.Rows.Clear();
            return dt;
        }

        public string updateReturn(DataRow drItem)
        {
            ResultEntity result = new ResultEntity();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                result = dal.UpdateDetail(drItem);
            }
            return result.MassageText;
        }

        public bool deleteReturn(int id)
        {
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                dal.DeleteDetail(id, HttpContext.Current.Session["ses_user_id"].ToString());
            }
            return true;
        }

        public List<string> SendReturnCompanyToHost(DataTable dt)
        {
            List<string> returnValue = new List<string>();
            ResultEntity result = new ResultEntity();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new ReturnCompanyDAL())
            {
                result = dal.SendToHost(dt);
            }

            if (result.ResultCode == 0)
            {
                returnValue.Add("");
                returnValue.Add("ส่งข้อมูลเรียบร้อยแล้ว!");
            }
            else
            {
                returnValue.Add("Error!");
                returnValue.Add(result.MassageText);
            }
            return returnValue;
        }


    }
}
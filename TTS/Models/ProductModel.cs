﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TTS.Controllers;

namespace TTS.Models
{
    public class ProductModel
    {
        public int DocId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }
        public string CompanyCode { get; set; }
        public string GroupBrandCode { get; set; }
        public string BrandName { get; set; }
        public string ProductTypeCode { get; set; }
        public string TypeName { get; set; }
        public string GroupProduct { get; set; }
        public string GroupName { get; set; }
        public string SubGroupProduct { get; set; }
        public string SubGroupName { get; set; }
        public string Barcode { get; set; }
        public string PGSC { get; set; }
        public Nullable<int> Revision { get; set; }
        public string Size { get; set; }
        public string Style { get; set; }
        public string Color { get; set; }
        public string YearStart { get; set; }
        public string PeriodStart { get; set; }
        public string Category { get; set; }
        public Nullable<decimal> RTPrice { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public string Active { get; set; }
   
    }

    public class Product
    {
        public ProductModel GetProductName(string Product, string SizeName)
        {
            try
            {
                DataTable dt = new DataTable();
                GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
                using (var dal = new ProductsDAL())
                {
                    dt = dal.GetProduct("", function.EmptyIfNull(Product), function.EmptyIfNull(SizeName));
                }

                ProductModel p = new ProductModel();
                if (dt.Rows.Count > 0)
                    p.Style = dt.Rows[0]["Style"].ToString();
                else
                    p.Style = "";
                return p;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
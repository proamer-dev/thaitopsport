﻿using DataLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TTS.Models
{
    public class WarehouseModel
    {
        public string WHCode { get; set; }
    }

    public class Warehouse
    {
        public List<WarehouseModel> getWarehouseList()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new WarehouseDAL())
            {
                dt = dal.GetWarehouseAll();
            }
            //var sector1Results = (from r in dt.AsEnumerable()
            //select r["BranchCode"]).Distinct().ToList(); ;
            dt.DefaultView.Sort = "WHCode ASC";
            var sector1Results = dt.DefaultView.ToTable(true, "WHCode");

            List<WarehouseModel> ListWarehouse = sector1Results.AsEnumerable().Select(m => new WarehouseModel()
            {
                WHCode = m.Field<string>("WHCode")
            }).ToList();

            return ListWarehouse;
        }
    }

}
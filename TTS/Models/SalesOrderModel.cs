﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataLibrary;
using System.Data;
using TTS.Controllers;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TTS.Models
{
    public class BranchFromSalesOrderModel
    {
        public string CustGroupCode { get; set; }
        public string BranchCode { get; set; }
        public string Description { get; set; } //Cust Group Name
        public string CustNameThai { get; set; }
        public int RecordsPerGroup { get; set; }
        public int TotalRecords { get; set; }
    }

    public class SalesOrderModel
    {
        public string CustGroupCode { get; set; }
        public string Description { get; set; } //Cust Group Name
        public string CustNameThai { get; set; }
        public int RecordsPerGroup { get; set; }
        public int TotalRecords { get; set; }

        //***********
        [Display(Name = "รหัสการขาย")]
        public string OrderNumber { get; set; }
        [Display(Name = "วันที่")]
        public System.DateTime DocDate { get; set; }
        public string strDocDate { get; set; }
        [Display(Name = "รหัสสาขา")]
        public string BranchCode { get; set; }
        public int LineNumber { get; set; }
        public string InvoiceNumber { get; set; }
        [Display(Name = "รหัสสินค้า")]
        public string ProductId { get; set; }
        [Display(Name = "ชื่อสินค้า")]
        public string ProductName { get; set; }

        [Display(Name = "ราคาป้าย")]
        public decimal Price { get; set; }

        public decimal Cost { get; set; }
        [Display(Name = "ส่วนลดบริษัท")]
        public decimal DiscountPer { get; set; }
        [Display(Name = "")]
        public decimal Discount { get; set; }
        public decimal DiscountOnTop { get; set; }
        [Display(Name = "ส่วนลดลูกค้าทั้งหมด")]
        public decimal DiscountAll { get; set; }
        [Display(Name = "ปริมาณ")]
        public int Qty { get; set; }

        [Display(Name = "จำนวนเงินรวม")]
        public decimal PriceTotal { get; set; }
        public decimal LineTotal { get; set; }
        public decimal Vat { get; set; }
        public decimal dGP { get; set; }

        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string style { get; set; }
        public string GroupBrandCode { get; set; }
        public string GPCode { get; set; }
        public string RefNumber { get; set; }
        public string IsExport { get; set; }

        [Display(Name = "ขนาด")]
        public string size_id { get; set; }
        public IEnumerable<SelectListItem> Size { get; set; }

        [Display(Name = "รหัส GP")]
        public string GP_id { get; set; }
        public IEnumerable<SelectListItem> GP { get; set; }
        public IEnumerable<SelectListItem> ListBranch { get; set; }

        public List<SalesOrderModel> ListSalesOrder { get; set; }


        public string OrderDate { get; set; }
        public string ToOrderDate { get; set; }
        public string DiscountCode { get; set; }
        public string DiscountName { get; set; }
    }

    public class SalesOrder
    {
        public IEnumerable<SalesOrderModel> getSalesOrder(string OrderNumber, string BranchCode, string ProductId, string SizeName, string OrderDate, string ToOrderDate, string IsExport)
        {
            #region session_Filter
            if (function.EmptyIfNull(OrderNumber) == "" & function.EmptyIfNull(BranchCode) == "" & function.EmptyIfNull(ProductId) == "" & function.EmptyIfNull(SizeName) == "" & (OrderDate == null || OrderDate == "") & (ToOrderDate == null || ToOrderDate == "") & function.EmptyIfNull(IsExport) == "")
            {
                if (HttpContext.Current.Session["ses_data_filter_SalesOrder"] != null)
                {
                    SalesOrderModel s = (SalesOrderModel)HttpContext.Current.Session["ses_data_filter_SalesOrder"];
                    OrderNumber = s.OrderNumber;
                    BranchCode = s.BranchCode;
                    ProductId = s.ProductId;
                    SizeName = s.size_id;
                    OrderDate = s.OrderDate;
                    ToOrderDate = s.ToOrderDate;
                    IsExport = s.IsExport;
                }
            }

            SalesOrderModel so = new SalesOrderModel();
            so.OrderNumber = OrderNumber;
            so.BranchCode = BranchCode;
            so.ProductId = ProductId;
            so.size_id = SizeName;
            so.OrderDate = OrderDate;
            so.ToOrderDate = ToOrderDate;
            so.IsExport = IsExport;
            HttpContext.Current.Session["ses_data_filter_SalesOrder"] = so;
            #endregion

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;

            //var myDate = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }


            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrder(function.EmptyIfNull(OrderNumber), function.EmptyIfNull(BranchCode), function.EmptyIfNull(ProductId), function.EmptyIfNull(SizeName), dateStart, dateStop, function.EmptyIfNull(IsExport));
            }

            //dt = cSQLExcute.ExecuteDataAdapter("SELECT * FROM v_SalesOrderDetail", CommandType.Text).Tables[0];

            List<SalesOrderModel> listSalesOrder = dt.AsEnumerable().Select(m => new SalesOrderModel()
            {
                //Display in Grid
                OrderNumber = m.Field<string>("OrderNumber"),
                DocDate = m.Field<DateTime>("DocDate"),
                BranchCode = m.Field<string>("BranchCode"),
                LineNumber = m.Field<int>("LineNumber"),
                InvoiceNumber = m.Field<string>("InvoiceNumber"),
                ProductId = m.Field<string>("ProductId"),
                //ProductName = m.Field<string>("ProductName"),

                Price = m.Field<decimal>("Price"),
                Cost = m.Field<decimal>("Cost"),
                DiscountPer = m.Field<decimal>("DiscountPer"),
                Discount = m.Field<decimal>("Discount"),
                Qty = m.Field<int>("Qty"),
                dGP = m.Field<decimal>("GP"),
                PriceTotal = m.Field<decimal>("PriceTotal"),
                LineTotal = m.Field<decimal>("LineTotal"),
                Vat = m.Field<decimal>("Vat"),
                size_id = m.Field<string>("Size"),
                GroupBrandCode = m.Field<string>("GroupBrandCode"),
                GPCode = m.Field<string>("GPCode"),
                IsExport = m.Field<string>("IsExport")
            }).ToList();

            return listSalesOrder;
        }

        public IEnumerable<SalesOrderModel> getSalesOrderByBranch(string BranchCode, string OrderDate, string ToOrderDate)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;

            //var myDate = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }


            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrder("", function.EmptyIfNull(BranchCode), "", "", dateStart, dateStop, "N");
            }

            //dt = cSQLExcute.ExecuteDataAdapter("SELECT * FROM v_SalesOrderDetail", CommandType.Text).Tables[0];

            List<SalesOrderModel> listSalesOrder = dt.AsEnumerable().Select(m => new SalesOrderModel()
            {
                //Display in Grid
                OrderNumber = m.Field<string>("OrderNumber"),
                DocDate = m.Field<DateTime>("DocDate"),
                BranchCode = m.Field<string>("BranchCode"),
                LineNumber = m.Field<int>("LineNumber"),
                InvoiceNumber = m.Field<string>("InvoiceNumber"),
                ProductId = m.Field<string>("ProductId"),
                //ProductName = m.Field<string>("ProductName"),

                Price = m.Field<decimal>("Price"),
                Cost = m.Field<decimal>("Cost"),
                DiscountPer = m.Field<decimal>("DiscountPer"),
                Discount = m.Field<decimal>("Discount"),
                Qty = m.Field<int>("Qty"),
                dGP = m.Field<decimal>("GP"),
                PriceTotal = m.Field<decimal>("PriceTotal"),
                LineTotal = m.Field<decimal>("LineTotal"),
                Vat = m.Field<decimal>("Vat"),
                size_id = m.Field<string>("Size"),
                GroupBrandCode = m.Field<string>("GroupBrandCode"),
                GPCode = m.Field<string>("GPCode"),
                IsExport = m.Field<string>("IsExport")
            }).ToList();

            return listSalesOrder;
        }


        //public string getSalesOrder(string custGroup, string BranchCode, string OrderDate, string ToOrderDate)
        //{
        //    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
        //    DataTable dt = new DataTable();
        //    DateTime dateStart;
        //    DateTime dateStop;

        //    if (OrderDate == null || OrderDate == "")
        //    {
        //        dateStart = DateTime.Now.AddDays(-1);
        //        dateStop = DateTime.Now;
        //    }
        //    else
        //    {
        //        dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
        //        dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
        //    }

        //    GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
        //    using (var dal = new SalesOrderDAL())
        //    {
        //        dt = dal.SelectBranchCodeToSalesOrder(function.EmptyIfNull(custGroup), function.EmptyIfNull(BranchCode), dateStart, dateStop);
        //    }

        //    var J = Controllers.function.DataTableToJSON(dt);

        //    //List<BranchFromSalesOrderModel> listSalesOrder = dt.AsEnumerable().Select(m => new BranchFromSalesOrderModel()
        //    //{
        //    //    //Display in Grid
        //    //    CustGroupCode = m.Field<string>("CustGroupCode"),
        //    //    Description = m.Field<string>("Description"),
        //    //    BranchCode = m.Field<string>("BranchCode"),
        //    //    CustNameThai = m.Field<string>("CustNameThai"),
        //    //    RecordsPerGroup = m.Field<int>("RecordsPerGroup"),
        //    //    TotalRecords = m.Field<int>("TotalRecords")
        //    //}).ToList();

        //    //return listSalesOrder;
        //    return J;
        //}


        public List<BranchFromSalesOrderModel> getSalesOrder(string custGroup, string BranchCode, string OrderDate, string ToOrderDate)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DateTime dateStart;
            DateTime dateStop;

            if (OrderDate == null || OrderDate == "")
            {
                dateStart = DateTime.Now.AddDays(-1);
                dateStop = DateTime.Now;
            }
            else
            {
                dateStart = DateTime.ParseExact(OrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                dateStop = DateTime.ParseExact(ToOrderDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
            }

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectBranchCodeToSalesOrder(function.EmptyIfNull(custGroup), function.EmptyIfNull(BranchCode), dateStart, dateStop);
            }

            List<BranchFromSalesOrderModel> listSalesOrder = dt.AsEnumerable().Select(m => new BranchFromSalesOrderModel()
            {
                //Display in Grid
                CustGroupCode = m.Field<string>("CustGroupCode"),
                Description = m.Field<string>("Description"),
                BranchCode = m.Field<string>("BranchCode"),
                CustNameThai = m.Field<string>("CustNameThai"),
                RecordsPerGroup = m.Field<int>("RecordsPerGroup"),
                TotalRecords = m.Field<int>("TotalRecords")
            }).ToList();

            return listSalesOrder;
        }



        public SalesOrderModel getSalesOrderDetail(string OrderNumber, string ProductId, string Size)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DataTable dtDiscount = new DataTable();
            DataTable dtDiscountExtra = new DataTable();

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetail(function.EmptyIfNull(OrderNumber), function.EmptyIfNull(ProductId), function.EmptyIfNull(Size));
            }

            if (dt.Rows.Count > 0)
            {
                using (var dal = new SalesOrderDAL())
                {
                    dtDiscount = dal.SelectSalesOrderDetailOnTopDis(dt.Rows[0]["OrderNumber"].ToString(), (int)dt.Rows[0]["LineNumber"]);
                    dtDiscountExtra = dal.SelectSalesOrderDetailOnTopDis(dt.Rows[0]["OrderNumber"].ToString(), (int)dt.Rows[0]["LineNumber"]);
                }

                SalesOrderModel s = new SalesOrderModel();
                s.OrderNumber = dt.Rows[0]["OrderNumber"].ToString();
                s.ProductId = dt.Rows[0]["ProductId"].ToString();
                s.LineNumber = (int)dt.Rows[0]["LineNumber"];
                s.size_id = dt.Rows[0]["Size"].ToString();
                s.Price = (decimal)dt.Rows[0]["Price"];
                s.Cost = (decimal)dt.Rows[0]["Cost"];
                s.DiscountPer = (decimal)dt.Rows[0]["DiscountPer"];
                s.Discount = (decimal)dt.Rows[0]["Discount"];
                s.DiscountOnTop = (decimal)dt.Rows[0]["DiscountOnTop"];
                s.GP_id = dt.Rows[0]["GP"].ToString();
                s.Qty = (int)dt.Rows[0]["Qty"];
                s.PriceTotal = (decimal)dt.Rows[0]["PriceTotal"];
                s.Vat = (decimal)dt.Rows[0]["Vat"];
                s.LineTotal = (decimal)dt.Rows[0]["LineTotal"];
                s.style = dt.Rows[0]["Style"].ToString();
                s.strDocDate = Convert.ToDateTime(dt.Rows[0]["DocDate"]).ToString("dd/MM/yyyy");
                s.GPCode = dt.Rows[0]["GPCode"].ToString();

                s.InvoiceNumber = dt.Rows[0]["InvoiceNumber"].ToString();
                s.RefNumber = dt.Rows[0]["RefNumber"].ToString();
                s.IsExport = dt.Rows[0]["IsExport"].ToString();


                return s;
            }
            else
                return null;
        }
        public DataTable getSalesOrderDetailDt(string OrderNumber, string ProductId, string Size)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            DataTable dtDiscount = new DataTable();
            DataTable dtDiscountExtra = new DataTable();

            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetail(function.EmptyIfNull(OrderNumber), function.EmptyIfNull(ProductId), function.EmptyIfNull(Size));
            }

            return dt;
        }

        public List<SalesOrderModel> getDiscountOnTop(string OrderNumber, int LineNumber)
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetailOnTopDis(OrderNumber, LineNumber);
            }

            List<SalesOrderModel> listSalesOrderDiscountExtra = dt.AsEnumerable().Select(m => new SalesOrderModel()
            {
                //Display in Grid
                DiscountCode = m.Field<string>("DisCode"),
                DiscountName = m.Field<string>("DisName"),
                DiscountPer = m.Field<decimal>("DiscountPer"),
                Discount = m.Field<decimal>("DiscountAm")
            }).ToList();

            return listSalesOrderDiscountExtra;
        }

        public List<SalesOrderModel> getDiscountExtra(string OrderNumber, int LineNumber)
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetailDiscountExtra(OrderNumber, LineNumber);
            }

            List<SalesOrderModel> listSalesOrderDiscountExtra = dt.AsEnumerable().Select(m => new SalesOrderModel()
            {
                //Display in Grid
                DiscountCode = m.Field<string>("DisCode"),
                DiscountName = m.Field<string>("DisName"),
                DiscountPer = m.Field<decimal>("DiscountPer"),
                Discount = m.Field<decimal>("DiscountAm")
            }).ToList();

            return listSalesOrderDiscountExtra;
        }
        
        public DataTable getSalesOrderDetailTable()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetail("0", "0", "");
            }

            if (dt.Rows.Count > 0)
                dt.Rows.Clear();
            return dt;
        }

        public DataTable getDiscountOnTopTable()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetailOnTopDis("0", 0);
            }

            if (dt.Rows.Count > 0)
                dt.Rows.Clear();
            return dt;
        }

        public DataTable getDiscountExtraTable()
        {
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.SelectSalesOrderDetailDiscountExtra("0", 0);
            }

            if (dt.Rows.Count > 0)
                dt.Rows.Clear();
            return dt;
        }

        public string updateSalesOrder(DataRow drItem, DataTable dtDiscountOnTop, DataTable dtDiscountExtra)
        {

            ResultEntity result = new ResultEntity();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                result = dal.UpdateDetail(drItem, dtDiscountOnTop, dtDiscountExtra, HttpContext.Current.Session["ses_user_id"].ToString());
            }
            return result.MassageText;
        }

        public bool deleteSalesOrder(DataRow drItem)
        {
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dal.DeleteDetail(drItem);
            }
            return true;
        }

        public List<string> SendSalesOrderToHost(DataTable dt)
        {
            List<string> returnValue = new List<string>();
            ResultEntity result = new ResultEntity();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                result = dal.SendSalesOrderToHost(dt);
            }

            if (result.ResultCode == 0)
            {
                returnValue.Add("");
                returnValue.Add("ส่งข้อมูลเรียบร้อยแล้ว!");
            }
            else
            {
                returnValue.Add("Error!");
                returnValue.Add(result.MassageText);
            }
            return returnValue;
        }

        public IEnumerable<DiscountModel> getGPList(string strPGSC, string strDiscountID)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(1033);
            DataTable dt = new DataTable();
            GlobalVariable.ConnectionString = HttpContext.Current.Session["ses_sys_db_connection"].ToString();
            using (var dal = new SalesOrderDAL())
            {
                dt = dal.GetDisCountsCom(strPGSC, strDiscountID);
            }

            List<DiscountModel> ListCouponList = dt.AsEnumerable().Select(m => new DiscountModel()
            {
                discountPer = m.Field<decimal>("DeptDiscount")
            })
            .ToList();
            return ListCouponList;
        }





        #region Return Customer

        #endregion
    }
}
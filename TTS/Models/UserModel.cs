﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Enterprise.HelperCSharp.Cryptography;
using System.Web.Mvc;

namespace TTS.Models
{
    public class UserModel
    {
        public string tran_id { get; set; }

        [Display(Name = "User ID")]
        [Required]
        public string user_id { get; set; }

        [Display(Name = "User Name")]
        [Required]
        public string name { get; set; }

        [Display(Name = "First Name")]
        [Required]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        public string last_name { get; set; }

        [Display(Name = "Password")]
        [Required]
        [StringLength(50)]
        //[RegularExpression("^.*(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$")]
        public string password { get; set; }


        public string locale_id { get; set; }

        [Display(Name = "Active")]
        public string is_active { get; set; }

        [Display(Name = "Department")]
        public string department { get; set; }
        public string supervisor { get; set; }
        public string email_address { get; set; }
        public string domain_user_id { get; set; }
        public string domain { get; set; }
        public System.DateTime create_date { get; set; }
        public string create_by { get; set; }
        public byte[] rowversion { get; set; }

        [Display(Name = "User Group")]
        public string user_group_id { get; set; }


        public IEnumerable<SelectListItem> UserGroup { get; set; }
    }

    public class User
    {
        public UserModel getUser_userID(string user_id)
        {
            try
            {
                Controllers.Site s = new Controllers.Site();
                SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@user_id", user_id)
                        };
                DataSet ds = cSQLExcute.ExecuteDataAdapter("usp_get_user", CommandType.StoredProcedure, Parameter);
                DataTable dt = ds.Tables[0];
                UserModel User = new UserModel();

                //Display in Grid
                User.user_id = dt.Rows[0]["user_id"].ToString();
                User.name = dt.Rows[0]["name"].ToString();
                User.first_name = dt.Rows[0]["first_name"].ToString();
                User.last_name = dt.Rows[0]["last_name"].ToString();
                User.locale_id = dt.Rows[0]["locale_id"].ToString();
                User.is_active = dt.Rows[0]["is_active"].ToString();
                User.create_date = Convert.ToDateTime(dt.Rows[0]["create_date"]);
                //more..
                User.tran_id = dt.Rows[0]["tran_id"].ToString();
                User.password = dt.Rows[0]["password"].ToString();
                User.department = dt.Rows[0]["department"].ToString();
                User.supervisor = dt.Rows[0]["supervisor"].ToString();
                User.email_address = dt.Rows[0]["email_address"].ToString();
                User.domain_user_id = dt.Rows[0]["domain_user_id"].ToString();
                User.domain = dt.Rows[0]["domain"].ToString();
                User.create_by = dt.Rows[0]["create_by"].ToString();

                //User Group
                User.user_group_id = dt.Rows[0]["user_group_id"].ToString();
                User.UserGroup = Controllers.function.ToSelectList(ds.Tables[1], "user_group_id", "name");

                return User;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public IEnumerable<UserModel> getUser(string strUserName, string strFirstName, string strLastName)
        {
            try
            {
                Controllers.Site s = new Controllers.Site();
                SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@name", Controllers.function.NullIfEmpty(strUserName)),
                             new SqlParameter("@first_name", Controllers.function.NullIfEmpty(strFirstName)),
                             new SqlParameter("@last_name", Controllers.function.NullIfEmpty(strLastName))
                        };
                DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_get_user", CommandType.StoredProcedure, Parameter).Tables[0];

                List<UserModel> listUser = dt.AsEnumerable().Select(m => new UserModel()
                {
                    //Display in Grid
                    user_id = m.Field<string>("user_id"),
                    name = m.Field<string>("name"),
                    first_name = m.Field<string>("first_name"),
                    last_name = m.Field<string>("last_name"),
                    locale_id = m.Field<string>("locale_id"),
                    is_active = m.Field<string>("is_active"),
                    create_date = m.Field<DateTime>("create_date"),
                    //more..
                    tran_id = m.Field<string>("tran_id"),
                    password = m.Field<string>("password"),
                    department = m.Field<string>("department"),
                    supervisor = m.Field<string>("supervisor"),
                    email_address = m.Field<string>("email_address"),
                    domain_user_id = m.Field<string>("domain_user_id"),
                    domain = m.Field<string>("domain"),
                    create_by = m.Field<string>("create_by")
                }).ToList();

                return listUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public SecurityService.ResultEntity updateUser(string user_id, string name, string first_name, string last_name, string password, string user_group_id)
        {
            string strPass = MD5.Decrypt(password, "OGA", true);
            strPass = (strPass == "" ? MD5.Encrypt(password, "OGA", true) : password);
            Controllers.Site s = new Controllers.Site();

            SqlParameter[] Parameter = new SqlParameter[] 
                        { 
                             new SqlParameter("@app_id", s.AppID),
                             new SqlParameter("@user_id", Controllers.function.NullIfEmpty(user_id)),
                             new SqlParameter("@name", Controllers.function.NullIfEmpty(name)),
                             new SqlParameter("@first_name", Controllers.function.NullIfEmpty(first_name)),
                             new SqlParameter("@last_name", Controllers.function.NullIfEmpty(last_name)),
                             new SqlParameter("@user_group_id", Controllers.function.NullIfEmpty(user_group_id)),
                             new SqlParameter("@create_by", HttpContext.Current.Session["ses_user_id"].ToString()),
                             new SqlParameter("@password", strPass)
                        };
            DataTable dt = cSQLExcute.ExecuteDataAdapter("usp_update_user", CommandType.StoredProcedure, Parameter).Tables[0];

            SecurityService.ResultEntity result = new SecurityService.ResultEntity();
            result.ResultCode = Convert.ToInt32(dt.Rows[0]["ResultCode"].ToString());
            return result;
        }
    }

}
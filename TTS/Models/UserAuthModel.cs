﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TTS.Models
{
    public class UserAuthModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember on this computer")]
        public bool RememberMe { get; set; }

        public static SecurityService.ResultEntity AuthenLogin(string User, string Pass)
        {
            UserAuthModel login = new UserAuthModel();
            return login.AuthenUser(User, Pass);
        }


        public SecurityService.ResultEntity AuthenUser(string User, string Pass)
        {
            try
            {
                using (var ser = new SecurityService.ServiceClient())
                {
                    Controllers.Site s = new Controllers.Site();
                    var resultAuthen = ser.LoginApplication(User,
                                                            Pass,
                                                            s.AppID,
                                                            s.DeviceName,
                                                            s.Platform);
                    return resultAuthen;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public SecurityService.ResultEntity LogoutUser()
        {
            try
            {
                Controllers.Site s = new Controllers.Site();

                using (var ser = new SecurityService.ServiceClient())
                {
                    SecurityService.UserLogonEntity ul = new SecurityService.UserLogonEntity();
                    ul.app_id = s.AppID;
                    ul.device = s.DeviceName;
                    ul.user_id = HttpContext.Current.Session["ses_user_id"].ToString();
                    var resultAuthen = ser.LogoutApplication(ul);

                    return resultAuthen;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public bool IsValid(string _UserName, string _Password)
        {
            return false;
        }
    }
}
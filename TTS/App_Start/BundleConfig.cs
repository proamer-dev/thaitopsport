﻿using System.Web;
using System.Web.Optimization;

namespace TTS
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-route.js",
                      
                        "~/Scripts/angular-ui/ui-bootstrap-tpls.js"
                        ));


            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-migrate-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Content/themes/Default/bootstrap/js/bootstrap.js",
                        "~/Scripts/gridmvc.js",
                        //"~/Scripts/gridmvc-ext.js",
                        //"~/Scripts/ladda-bootstrap/ladda.min.js",
                        //"~/Scripts/ladda-bootstrap/spin.min.js"
                        "~/Scripts/bootbox.js",
                        "~/Content/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
                        "~/Content/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js",
                        "~/Content/plugins/timeout-dialog.js/js/timeout-dialog.js"
                        ));


            bundles.Add(new ScriptBundle("~/bundles/AdminLTE").Include(
                       "~/Content/themes/Default/dist/js/app.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/AdminLTE_Script").Include(
                       "~/Content/themes/Default/dist/js/script.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/iCheck").Include(
                       "~/Content/plugins/iCheck/icheck.js"
                       ));


            bundles.Add(new ScriptBundle("~/bundles/fakeLoader").Include(
                       "~/Content/plugins/fakeLoader/js/fakeLoader.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                       "~/Content/plugins/select2/select2.js"
                       ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/Default").Include(
                        "~/Content/themes/Default/bootstrap/css/bootstrap.css",
                        "~/Content/font-awesome-4.4.0/css/font-awesome.css",
                        "~/Content/themes/Default/dist/css/AdminLTE.css",
                        "~/Content/themes/Default/dist/css/skins/_all-skins.css",
                        "~/Content/plugins/iCheck/square/blue.css",
                        "~/Content/themes/Default/Style.css",
                        "~/Content/Gridmvc.css",
                        "~/Content/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css",
                        "~/Content/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css",
                        //"~/Content/ladda-bootstrap/ladda-themeless.min.css"
                        "~/Content/plugins/select2/select2.css",
                        "~/Content/plugins/loading-spin/preloading.css",
                        "~/Content/plugins/timeout-dialog.js/css/timeout-dialog.css"
                        //"~/Content/plugins/fakeLoader/css/fakeLoader.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/site.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TTS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "CallAction",
            //    url: "{controller}/{action}/",
            //    defaults: new
            //    {
            //        controller = "Process",
            //        action = "SalesOrder"
            //    }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {
                    controller = "Home", 
                    action = "Index", 
                    id = UrlParameter.Optional 
                }
            );


            routes.MapRoute(
                name: "getBranchFromSalesOrder",
                url: "{controller}/{action}/{bid}/{start}/{stop}",
                defaults: new
                {
                    controller = "Process",
                    action = "SalesOrder",
                    bid = UrlParameter.Optional,
                    start = UrlParameter.Optional,
                    stop = UrlParameter.Optional
                }
            );

            //Process/SalesOrder_Edit?i=128201512160001&p=1-V46830&s=08&g=01
            routes.MapRoute(
                name: "SalesOrderEdit",
                url: "{controller}/{action}/{i}/{p}/{s}/{g}",
                defaults: new
                {
                    controller = "Process",
                    action = "SalesOrder_Edit",
                    i = UrlParameter.Optional,
                    p = UrlParameter.Optional,
                    s = UrlParameter.Optional,
                    g = UrlParameter.Optional
                }
            );

        }
    }
}
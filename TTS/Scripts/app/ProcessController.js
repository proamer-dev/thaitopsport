﻿var app = angular.module('EsalesApp', ['ngRoute', 'ui.bootstrap']);

app.config(function ($locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
});

//app.config(['$routeProvider',
//  function ($routeProvider) {
//      $routeProvider.
//        when('/Process', {
//            templateUrl: '/Process',
//            controller: 'ProcessController'
//        });
//        //otherwise({
//        //    redirectTo: '/addOrder'
//        //});
//}]);

app.directive('salesOrderRepeatDirective', function () {
      return function (scope, element, attrs) {
          if (scope.$last) {
              $('input').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  increaseArea: '20%' // optional
              });

              var oTable = $('#tbSalesOrder').DataTable({
                  //data: dataSet,
                  lengthMenu: [[10, 20, 50, 100, -1], [10, 20, 50, 100, "All"]],
                  pageLength: 20,

                  "autoWidth": false,
                  //"aaSorting": [[1, 'asc']],
                  "dom": 'rt<"row"<"col-md-6"li><"col-md-6"p>> <"clear">',
                  'aoColumnDefs': [{
                      'bSortable': false,
                      'aTargets': ['nosort']
                  }]
              });
          }
      };
  });

//app.config(['$routeProvider',
//  function ($routeProvider) {
//      $routeProvider.
//        when('/Process', {
//            templateUrl: 'partials/phone-list.html',
//            controller: 'ProcessController'
//        }).
//        when('/Process/:phoneId', {
//            templateUrl: 'partials/phone-detail.html',
//            controller: 'ProcessController'
//        }).
//        otherwise({
//            redirectTo: '/Process'
//        });
//  }]);

app.controller('ProcessController', function ($scope, $http, $filter) {
    //filter
    var arrBranch = new Array();
    $http.get("getBranch/").success(function (data) {
        $.map(data, function (item) {
            arrBranch.push(item.BranchName);
        });
        $scope.bList = arrBranch;
    }).error(function (status) {
        //alert(status);
        //var win = window.open("", "Title", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=780, height=200, top=" + (screen.height - 400) + ", left=" + (screen.width - 840));
        //win.document.body.innerHTML = status;
    });

    var arrSize = new Array();
    $http.get("getSize/").success(function (data) {
        $.map(data, function (item) {
            arrSize.push(item.name);
        });
        $scope.sList = arrSize;
    }).error(function (status) {
        //alert(status);
    });

    //var arrCustGroup = new Array();
    $http.get("getCustCode/").success(function (data) {
        $scope.cList = data;
    }).error(function (status) {
        //alert(status);
    });

    //**Search Branch
    $scope.custCodeList_OnChange = function () {
        $("#div_spin").show();

        $http.get("getBranchByCustGroup/", { params: { CustGroupCode: $scope.custCodeList } })
            .success(function (data) {
                $scope.baList = data;
                //$scope.branchList.selected = undefined;
                //$scope.clear();

                $("#lsBranch").val("");
                //$('#lsBranch option:first-child').attr("selected", "selected");
                //$("#lsBranch").select2("val", "");
        })
            .error(function (status) {
        });
        $("#div_spin").hide();
    };

    $scope.clear = function ($event) {
        $event.stopPropagation();
        $scope.branchList.selected = undefined;
    };



    $scope.alerts = [
      //{ type: 'error', msg: 'Oh snap! Change a few things up and try submitting again.', show: true }
      //{ type: 'success', msg: 'Well done! You successfully read this important alert message.', show: false }
    ];

    $scope.addAlert = function (alertType, message) {
        $scope.closeAllAlert();
        $scope.alerts.push({ type: alertType, msg: message, show: true });
    };

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.closeAllAlert = function () {
        var newAlerts = [];
        $scope.alerts = newAlerts;
    };


    //
    $scope.btnSearchProduct_OnClick = function () {
        if ($scope.productID != undefined) {
            $http.get("getProductName/", { params: { Product: $scope.productID, Size: $scope.sizeList } }).success(function (data) {
                $scope.productName = data.Style;
            });
        }
    };

    $scope.btnSendToHost = function () {
        $("#loader-wrapper").fadeIn();
        //$http.get("Process/SalesOrder_Send/").success(function (data) {
        //    if (data[0] == "") {
        //        $scope.addAlert("success", data[1]);
        //    } else {
        //        $scope.addAlert("error", data[1]);
        //    }
        //})
        //$("#loader-wrapper").hide();
    };

    $scope.btnSendCustomerToHost = function () {
        $("#loader-wrapper").show();
        $http.get("ReturnCustomer_Send/").success(function (data) {
            if (data[0] == "") {
                $scope.addAlert("success", data[1]);
            } else {
                $scope.addAlert("error", data[1]);
            }
        })
        $("#loader-wrapper").hide();
    };

    $scope.btnSendCompanyToHost = function () {
        $("#loader-wrapper").show();
        $http.get("ReturnCompany_Send/").success(function (data) {
            if (data[0] == "") {
                $scope.addAlert("success", data[1]);
            } else {
                $scope.addAlert("error", data[1]);
            }
        })
        $("#loader-wrapper").hide();
    };

    var dataSet = [
    ["Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800"],
    ["Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750"],
    ["Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000"]];


    //Main Data
    $scope.HOChoosed = function () {
        //var trues = $filter("filter")($scope.BranchFromSalesOrderList, { val: true });
        //return trues.length;
        return true;
    }

    //SalesOrder
    $scope.getBranchFromSalesOrder = function () {
        $("#loader-wrapper").fadeIn();
        //$cookieStore.put("sesCustCode_SalesOrder", $scope.custCodeList);
        //$cookieStore.put("sesBranchCode_SalesOrder", $scope.branchList);
        //$cookieStore.put("sesStart_SalesOrder", $scope.period_start);
        //$cookieStore.put("sesStop_SalesOrder", $scope.period_end);

        $http.get("getSalesOrder/", { params: { custGroup: $scope.custCodeList, branchCode: $scope.branchList, start: $scope.period_start, stop: $scope.period_end } }).success(function (data) {

            $scope.BranchFromSalesOrderList = data;
        }).error(function (status) {
            //alert(status);
        }).finally(function () {
            $("#loader-wrapper").hide();
        });
    }

    //Return Company
    $scope.getBranchFromReturnCompany = function () {
        $("#loader-wrapper").fadeIn();
        $http.get("getReturnCompany/", { params: { custGroup: $scope.custCodeList, branchCode: $scope.branchList, start: $scope.period_start, stop: $scope.period_end } }).success(function (data) {

            $scope.BranchFromSalesOrderList = data;

        }).error(function (status) {
            //alert(status);
        }).finally(function () {
            $("#loader-wrapper").hide();
        });
    }

    //Return from Customer
    $scope.getBranchFromReturnCustomer = function () {
        $("#loader-wrapper").fadeIn();
        $http.get("getReturnCompany/", { params: { custGroup: $scope.custCodeList, branchCode: $scope.branchList, start: $scope.period_start, stop: $scope.period_end } }).success(function (data) {

            $scope.BranchFromSalesOrderList = data;

        }).error(function (status) {
            //alert(status);
        }).finally(function () {
            $("#loader-wrapper").hide();
        });
    }
});
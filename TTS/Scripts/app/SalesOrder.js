﻿
app.controller('ProcessController_Edit', ['$scope', '$location', '$http', function ($scope, $location, $http) {

    var obj = $location.search();
    var objDiscountExtra = []; //รายการส่วนลด 
    var objDiscount = []; //รายการส่วนลด

    $scope.init_SalesOrder = function () {
        $scope.getGPList();
        $scope.getCouponDiscount();
        $scope.getSalesOrderDetail();
        $scope.getDiscountOnTop();
        $scope.getDiscountExtra();
    }


    $scope.getGPList = function () {
        var arrGP = new Array();
        $http.get("getGP/", { params: { BranchCode: obj['g'] } }).success(function (data) {
            $.map(data, function (item) {
                arrGP.push(item.gp);
            });
            $scope.glist = arrGP;

        }).error(function (status) {
            alert(status);
        });
    }


    $scope.getSalesOrderDetail = function () {
        $("#loader-wrapper").fadeIn();

        $http.get("getSalesOrderDetail/", { params: { id: obj['i'], p: obj['p'], s: obj['s'] } }).success(function (data) {
            $scope.OrderNumber = data.OrderNumber;//OrderNumber
            $scope.ProductId = data.ProductId;
            $scope.LineNumber = data.LineNumber;//LineNumber
            $scope.size_id = data.size_id;
            $scope.Price = data.Price;
            $scope.Cost = data.Cost;
            $scope.DiscountPer = data.DiscountPer;
            $scope.Discount = data.Discount;
            $scope.DiscountOnTop = data.DiscountOnTop;
            $scope.GP = data.GP_id;//GP
            $scope.Qty = data.Qty;
            $scope.PriceTotal = data.PriceTotal;
            $scope.Vat = data.Vat;//Vat
            $scope.LineTotal = data.LineTotal;//LineTotal
            $scope.Style = data.style;
            $scope.DocDate = data.strDocDate;
            $scope.GPlist = data.GPCode;
            $scope.DiscountAll = data.DiscountAll;//
            $scope.InvoiceNumber = data.InvoiceNumber;
            $scope.RefNumber = data.RefNumber;
            $scope.IsExport = data.IsExport;
            //$scope.SalePrice = 0;

        }).error(function (status) {
            alert(status);
        }).finally(function () {
            $("#loader-wrapper").hide();
        });
    }


    $scope.getCouponDiscount = function () {
        //Discount
        var objCouponlist;
        $http.get("getCoupon/").success(function (data) {
            $scope.Couponlists = data;
            objCouponlist = data;
        }).error(function (status) {
            alert(status);
        });

        //Discount Extra
        //var arrCouponExtra = new Array();
        var objCouponExtralists;
        $http.get("getCouponExtra/").success(function (data) {
            objCouponExtralists = data;
            $scope.CouponExtralists = data;
        }).error(function (status) {
            alert(status);
        });
    }


    $scope.Couponlist_OnChange = function (lsCoupon) {
        $http.get("getCouponByCode/", { params: { b: $scope.Couponlist } }).success(function (data) {
            $scope.dDiscountPer = data[0].discountPer;
            $scope.dDiscountBaht = data[0].discount;
        });
    };

    $scope.rowsDiscount = [];
    $scope.rowsDiscountValue = [];
    var i = 1;

    //Bind Discount On TOP
    $scope.getDiscountOnTop= function () {
        if (obj['i'] != null) {
            $http.get("getDiscountOnTop/", { params: { OrderNumber: obj['i'], LineNumber: obj['l'] } }).success(function (data) {
                for (var i = 0; i < data.length; i++) {
                    $scope.rowsDiscount.push({ 'No': j, 'DiscountPer': data[i].DiscountPer, 'DiscountBaht': data[i].Discount });

                    $scope.rowsDiscountValue.push({ 'Code': data[i].DiscountCode, 'name': data[i].DiscountName, 'DiscountPer': data[i].DiscountPer, 'DiscountBaht': data[i].Discount });

                    objDiscount.push({ 'Code': data[i].DiscountCode, 'DiscountPer': data[i].DiscountPer, 'DiscountBaht': data[i].Discount });
                    j++;
                }
            });
        }
    };

    $scope.addDiscount = function () {
        if (objDiscount.length == 0) {
            $scope.rowsDiscount.push({ 'No': i, 'DiscountPer': $scope.dDiscountPer, 'DiscountBaht': $scope.dDiscountBaht });
            //for Passing
            var lsCouponId = $scope.Couponlist;
            var lsCouponName = $.grep($scope.Couponlists, function (lsCoupon) {
                return lsCoupon.id == lsCouponId;
            })[0].name;
            $scope.rowsDiscountValue.push({ 'Code': $scope.Couponlist, 'name': lsCouponName.trim(), 'DiscountPer': $scope.dDiscountPer, 'DiscountBaht': $scope.dDiscountBaht });

            objDiscount.push({ 'Code': $scope.Couponlist, 'DiscountPer': $scope.dDiscountPer, 'DiscountBaht': $scope.dDiscountBaht });

            $scope.Couponlist[0];
            $scope.dDiscountPer = '';
            $scope.dDiscountBaht = '';
            i++;

            $scope.CalTotal();
        }
    };

    $scope.removeDiscount = function (idx) {
        //var discount_to_delete = $scope.persons[idx];
        $scope.rowsDiscount.splice(idx, 1);
        $scope.rowsDiscountValue.splice(idx, 1);
        objDiscount.splice(idx, 1);

        $scope.CalTotal();
    };


    $scope.CouponExtralist_OnChange = function () {
        $http.get("getCouponExtraByCode/", { params: { b: $scope.CouponExtralist } }).success(function (data) {
            $scope.dDiscountPerExtra = data[0].discountPer;
            $scope.dDiscountBahtExtra = data[0].discount;
        });
    };

    $scope.rowsDiscountExtra = [];
    $scope.rowsDiscountExtraValue = [];
    var j = 1;

    //Bind Discount Extra
    $scope.getDiscountExtra = function () {
        if (obj['i'] != null) {
            $http.get("getDiscountExtra/", { params: { OrderNumber: obj['i'], LineNumber: obj['l'] } }).success(function (data) {
                for (var i = 0; i < data.length; i++) {
                    $scope.rowsDiscountExtra.push({ 'No': j, 'DiscountPer': data[i].DiscountPer, 'DiscountBaht': data[i].Discount });

                    $scope.rowsDiscountExtraValue.push({ 'Code': data[i].DiscountCode, 'name': data[i].DiscountName, 'DiscountPer': data[i].DiscountPer, 'DiscountBaht': data[i].Discount });

                    objDiscountExtra.push({ 'Code': data[i].DiscountCode, 'DiscountPer': data[i].DiscountPer, 'DiscountBaht': data[i].Discount });
                    j++;
                }

            });
        }
    };


    $scope.addDiscountExtra = function () {
        $scope.rowsDiscountExtra.push({ 'No': j, 'DiscountPer': $scope.dDiscountPerExtra, 'DiscountBaht': $scope.dDiscountBahtExtra });
        //$scope.rowsDiscountExtraValue.push({ 'CouponExtralist': $scope.CouponExtralist });

        //for Passing
        var lsCouponId = $scope.CouponExtralist;
        var lsCouponName = $.grep($scope.CouponExtralists, function (lsCouponExtra) {
            return lsCouponExtra.id == lsCouponId;
        })[0].name;
        $scope.rowsDiscountExtraValue.push({ 'Code': $scope.CouponExtralist, 'name': lsCouponName.trim(), 'DiscountPer': $scope.dDiscountPerExtra, 'DiscountBaht': $scope.dDiscountBahtExtra });

        objDiscountExtra.push({ 'Code': $scope.CouponExtralist, 'DiscountPer': $scope.dDiscountPerExtra, 'DiscountBaht': $scope.dDiscountBahtExtra });

        $scope.CouponExtralist[0];
        $scope.dDiscountPerExtra = '';
        $scope.dDiscountBahtExtra = '';
        j++;

        $scope.CalTotal();
    };

    $scope.removeDiscountExtra = function (idx) {
        //var discount_to_delete = $scope.persons[idx];
        $scope.rowsDiscountExtra.splice(idx, 1);
        $scope.rowsDiscountExtraValue.splice(idx, 1);
        objDiscountExtra.splice(idx, 1);

        $scope.CalTotal();
    };

    //Update
    $scope.btnSaveSalesOrder_OnClick = function () {
        var objSalesOrder = {
            "OrderNumber": $scope.OrderNumber,
            "ProductId": $scope.ProductId,
            "LineNumber": $scope.LineNumber,
            "size_id": $scope.size_id,
            "Price": $scope.Price,
            "Cost": $scope.Cost,
            "DiscountPer": $scope.DiscountPer,
            "DiscountOnTop": $scope.Discount,
            "GP": $scope.GP,
            "Qty": $scope.Qty,
            "PriceTotal": $scope.PriceTotal,
            "Vat": $scope.Vat,
            "LineTotal": $scope.LineTotal,
            "Style": $scope.Style,
            "DocDate": $scope.DocDate,
            "GPCode": $scope.GPlist
        };

        //var data = $.param({
        //    SalesOrder: objSalesOrder,
        //    Discount: objDiscount,
        //    DiscountExtra: objDiscountExtra
        //});

        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }

        var arrSalesOrder = [];
        arrSalesOrder.push(objSalesOrder);

        var objSales = new Object();
        objSales = objSalesOrder;

        var dataObject = [];
        dataObject.push(arrSalesOrder);
        dataObject.push(objDiscount);
        dataObject.push(objDiscountExtra);

        var getObject = new Object();
        getObject.SalesOrder = dataObject;

        //$http.post('/Process/setSalesOrderDetail/', data, config)
        //    .success(function (data, status, headers, config) {
        //        //$scope.ServerResponse = data;
        //        alert("Success");
        //    })
        //    .error(function (data, status, header, config) {
        //        $scope.ServerResponse = "Data: " + data +
        //            "<hr />status: " + status +
        //            "<hr />headers: " + header +
        //            "<hr />config: " + config;
        //    });

        $http.post('/Process/setSalesOrderDetail/', JSON.stringify({
            SalesOrder: objSales
        }), JSON.stringify({
            Discount: objDiscount
        }), JSON.stringify({
            DiscountExtra: objDiscountExtra
        })).success(function (data, status, headers, config) {
            if (data != '' || data.length >= 0) {

            }
            else if (data == '') {
                //$scope.selectedIssue.Description = "";
            } else {
                //$scope.errors.push(data.error);
            }
        }).error(function (data, status, header, config) {
            $scope.ServerResponse = "Data: " + data +
                "<hr />status: " + status +
                "<hr />headers: " + header +
                "<hr />config: " + config;
        });

        //$http({
        //    url: "/Process/setSalesOrderDetail/",
        //    dataType: 'json',
        //    method: 'POST',
        //    data: getObject,
        //    headers: {
        //        "Content-Type": "application/json"
        //    }
        //}).success(function (response) {
        //    $scope.value = response;
        //})
        //   .error(function (error) {
        //       alert(error);
        //   });
    };


    $scope.CalTotal = function () {
        var Price = document.getElementsByName("SalePrice")[0].value; //ราคาขาย
        var resultDiscount = 0;
        var resultDiscountExtra = 0;
        for (var i = 0; i < objDiscount.length; i++) {
            if (objDiscount[i]["DiscountPer"] != 0) {
                resultDiscount += ((Price * objDiscount[i]["DiscountPer"]) / 100);
            }
            if (objDiscount[i]["DiscountBaht"] != 0) {
                resultDiscount += objDiscount[i]["DiscountBaht"];
            }
        }
        for (var i = 0; i < objDiscountExtra.length; i++) {
            if (objDiscountExtra[i]["DiscountPer"] != 0) {
                resultDiscountExtra += ((Price * objDiscountExtra[i]["DiscountPer"]) / 100);
            }
            if (objDiscountExtra[i]["DiscountBaht"] != 0) {
                resultDiscountExtra += objDiscountExtra[i]["DiscountBaht"];
            }
        }

        $scope.DiscountAll = resultDiscount + resultDiscountExtra;
        $scope.PriceTotal = (Price - (resultDiscount + resultDiscountExtra)) * $scope.Qty;
    }

}
]);


﻿

app.controller('ProcessController_ReturnCustomerEdit', ['$scope', '$location', '$http', function ($scope, $location, $http) {

    var obj = $location.search();

    $http.get("getReturnCustomerDetail/", { params: { id: obj['i'] } }).success(function (data) {
        $scope.Id = data.Id;
        $scope.PGSC = data.PGSC;
        $scope.Size = data.Size;
        $scope.Style = data.Style;

        $scope.LineNumber = data.LineNumber;//LineNumber
        $scope.OrderNumber = data.OrderNumber;//OrderNumber

        $scope.DiscountPer = data.DiscountPer;
        $scope.Discount = data.Discount;
        $scope.DiscountOnTop = data.DiscountOnTop;
        $scope.DiscountHdPer = data.DiscountHdPer;
        $scope.DiscountHdAm = data.DiscountHdAm;
        $scope.GP = data.GP;//GP
        $scope.Qty = data.Qty;
        $scope.Price = data.Price;
        $scope.PriceTotal = data.PriceTotal;
        $scope.Vat = data.Vat;//Vat
        $scope.LineTotal = data.LineTotal;//LineTotal

        $scope.DocDate = data.strDocDate;
        $scope.GPCode = data.GPCode;
        //$scope.DiscountAll = data.DiscountAll;//
        $scope.InvoiceNumber = data.InvoiceNumber;
        $scope.RefNumber = data.RefNumber;
        $scope.IsExport = data.IsExport;

        $scope.WHCode = data.WHCode;
        $scope.CompanyCode = data.CompanyCode;
        $scope.BranchCode = data.BranchCode;

    }).error(function (status) {
        alert(status);
    });

}
]);
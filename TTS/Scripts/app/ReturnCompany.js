﻿
app.controller('ProcessController_ReturnEdit', ['$scope', '$location', '$http', function ($scope, $location, $http) {
    var obj = $location.search();

    //WH
    var objWHlists;
    $http.get("getWH/").success(function (data) {
        $scope.WHlists = data;
        objWHlists = data;
    }).error(function (status) {
        //alert(status);
    });

    $http.get("getReturnDetail/", { params: { id: obj['i'] } }).success(function (data) {
        $scope.Id = data.Id;
        $scope.OrderNumber = data.OrderNumber;//OrderNumber
        $scope.ProductId = data.ProductId;
        $scope.ProductName = data.ProductName;
        $scope.Size = data.Size;

        $scope.Qty = data.Qty;

        $scope.OrderDate = data.OrderDate;
        $scope.BranchCode = data.BranchCode;
        $scope.WHCode = data.WHCode;

        $scope.ApproveBy = data.ApproveBy;
        $scope.Reason = data.Reason;
        $scope.RefNumber = data.RefNumber;
        $scope.IsExport = data.IsExport;
        $scope.InvoiceNumber = data.InvoiceNumber;
        //$scope.WHlists = data.WHCode;
        $scope.RTPrice = data.RTPrice;

    }).error(function (status) {
        alert(status);
    });


}]);
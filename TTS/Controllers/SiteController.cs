﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace TTS.Controllers
{
    public class SiteController : Controller
    {
        //
        // GET: /Site/

        //public static string strConnection = Session["ses_sys_db_connection"].ToString();
        //public string strConnection
        //{
        //    get { return HttpContext.Current.Session["ses_sys_db_connection"].ToString(); }
        //}

        public static string AppID { get; set; }

        public static string DeviceName { get; set; }

        public static string Platform { get; set; }

        static SiteController()
        {
            string IP = System.Web.HttpContext.Current.Request.UserHostName;
            string compName = DetermineCompName(IP);

            AppID = WebConfigurationManager.AppSettings["ApplicationID"];
            DeviceName = compName;
            Platform = "WEB";

        }

        public static string DetermineCompName(string IP)
        {
            IPAddress myIP = IPAddress.Parse(IP);
            IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
            List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();
            return compName.First();
        }
    }



    public class Site
    {
        //public string AppID { get; set; }
        //public string DeviceName { get; set; }
        //public string Platform { get; set; }

        private string _AppID;

        public string AppID
        {
            get { return WebConfigurationManager.AppSettings["ApplicationID"]; ; }
            set { _AppID = value; }
        }

        private string _DeviceName;

        public string DeviceName
        {
            get
            {
                string IP = System.Web.HttpContext.Current.Request.UserHostName;
                string compName = getDetermineCompName(IP);
                return compName;
            }
            set
            {
                _DeviceName = value;
            }
        }

        private string _Platform;

        public string Platform
        {
            get { return "WEB"; }
            set { _Platform = value; }
        }

        public string getDetermineCompName(string IP)
        {
            IPAddress myIP = IPAddress.Parse(IP);
            IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
            List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();
            return compName.First();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTS.Controllers
{
    public class UserControlController : Controller
    {
        //
        // GET: /UserControl/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListBranch()
        {
            Models.BranchModel objModel = new Models.BranchModel();
            Models.Branch objBranch = new Models.Branch();
            objModel.Branch = objBranch.getBranchAll();
            return View(objModel);
        }

    }
}

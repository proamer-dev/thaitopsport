﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;
using TTS.Models;

namespace TTS.Controllers
{
    public class ManagementController : Controller
    {
        //
        // GET: /Management/

        public ActionResult Index()
        {
            return View();
        }

        //*************  User  ********************
        public ActionResult User(string user_name, string user_fist_name, string user_last_name)
        {
            Models.User u = new User();
            return View(u.getUser(user_name, user_fist_name, user_last_name));
        }

        public ActionResult User_Add()
        {
            Models.UserModel u = new UserModel();
            Models.UserGroup ug = new UserGroup();
            u.UserGroup = function.ToSelectList(ug.getUserGroup_dt("", ""), "user_group_id", "name");
            return View("User_Edit", u);
        }

        public ActionResult User_Edit(string id)
        {
            Models.User u = new User();
            return View(u.getUser_userID(id));
        }

        public ActionResult User_Update(string user_id, string name, string first_name, string last_name, string password, string user_group_id)
        {
            Models.User u = new User();
            var Result = u.updateUser(user_id, name, first_name, last_name, password, user_group_id);
            if (Result.ResultCode == 0)
            {
                return RedirectToAction("User");
            }
            else
                return RedirectToAction("User_Edit", new { id = user_id, msg = Result.MessageText });//Return Error 
        }


        //*************  User Group  ********************
        public ActionResult UserGroup(string group_name, string group_desc)
        {
            Models.UserGroup u = new UserGroup();
            return View(u.getUserGroup(group_name, group_desc));
        }

        public ActionResult UserGroup_Add()
        {
            Models.UserGroupModel u = new UserGroupModel();
            return View("UserGroup_Edit", u);
        }

        public ActionResult UserGroup_Edit(string id)
        {
            Models.UserGroup u = new UserGroup();
            return View(u.getUserGroup_groupID(id));
        }

        public ActionResult UserGroup_Update(string user_group_id, string name, string description)
        {
            Models.UserGroup u = new UserGroup();
            var Result = u.updateUserGroup(user_group_id, name, description);
            if (Result.ResultCode == 0)
            {
                return RedirectToAction("UserGroup");
            }
            else
                return RedirectToAction("UserGroup_Edit", new { id = user_group_id, msg = Result.MessageText });//Return Error 
        }
        public ActionResult UserGroup_Delete(string id)
        {
            Models.UserGroup u = new UserGroup();
            u.deleteUserGroup(id);
            return View("UserGroup", u.getUserGroup("", ""));
        }




        //*************  User Authentication  ********************
        public ActionResult Authorize()
        {
            Models.UserModel u = new UserModel();
            Models.UserGroup ug = new UserGroup();
            u.UserGroup = function.ToSelectList(ug.getUserGroup_dt("", ""), "user_group_id", "name");
            return View(u);
        }


        [HttpGet]
        public ActionResult Authorize_Edit(string id)
        {
            #region Example
            // ตัวอย่าง
            // List<AuthorizeParentMenuModel> model = new List<AuthorizeParentMenuModel>();
            // AuthorizeParentMenuModel firstAuthor = new AuthorizeParentMenuModel
            // {
            //     menu_id = "111111",
            //     menu_name = "John",
            //     is_active = true,
            //     AuthorizeChildMenuModel = new List<AuthorizeChildMenuModel>{  
            //         new AuthorizeChildMenuModel{  
            //             menu_id="00001",  
            //             menu_name = "User",
            //             is_active = true
            //         }, new AuthorizeChildMenuModel{
            //             menu_id="00002",
            //             menu_name = "UserGroup",
            //             is_active = true  
            //         } , new AuthorizeChildMenuModel{  
            //             menu_id="00003",  
            //             menu_name = "Assign",  
            //             is_active = true  
            //         }  
            //     }
            // };
            // AuthorizeParentMenuModel secAuthor = new AuthorizeParentMenuModel
            //{
            //    menu_id = "2222",
            //    menu_name = "SalesOrder",
            //    is_active = true,

            //};
            // AuthorizeParentMenuModel trdAuthor = new AuthorizeParentMenuModel
            //{
            //    menu_id = "3333",
            //    menu_name = "Return",
            //    is_active = false

            //};

            // model.Add(firstAuthor);
            // model.Add(secAuthor);
            // model.Add(trdAuthor);
            #endregion

            Authorize auth = new Authorize();
            return View("Authorize_Edit", auth.getUserGroupMenu(id));
        }

        [HttpPost]
        public ActionResult Authorize_Edit(List<AuthorizeParentMenuModel> model)
        {
            List<AuthorizeParentMenuModel> selectedParent = model.Where(a => a.is_active).ToList();
            List<AuthorizeChildMenuModel> selectedChild = model.Where(a => a.is_active)
                                                .SelectMany(a => a.AuthorizeChildMenuModel.Where(b => b.is_active)).ToList();
            return View();
        }


        [HttpPost]
        public ActionResult Authorize_Update(FormCollection collection)
        {
            string strGoup_menu_id = Request.Params["id"];
            string[] arrResultChk;
            List<Models.AuthorizeParentMenuModel> lsPost = new List<Models.AuthorizeParentMenuModel>();

            //string strResult = "";
            arrResultChk = collection.GetValues("chk");

            //form post to List
            foreach (string s in Request.Params.Keys)
            {
                var IsMatch = Array.FindAll(arrResultChk, a => a.Equals(s.ToString()));
                if (IsMatch.Length > 0)
                {
                    //strResult += "name : " + s.ToString() + ", Value : " + Request.Params[s] + "\r\n";
                    Models.AuthorizeParentMenuModel apModel = new AuthorizeParentMenuModel();
                    apModel.menu_group_id = Request.Params["id"];
                    apModel.menu_id = s.ToString();
                    apModel.str_is_active = Request.Params[s];
                    lsPost.Add(apModel);
                }
            }

            //Array MenuID to List
            for (int i = 0; i < arrResultChk.Length; i++)
            {
                var IsMatch = lsPost.Find(x => x.menu_id == arrResultChk[i]);
                if (IsMatch == null) //Insert 'no' isNull
                {
                    Models.AuthorizeParentMenuModel apModel = new AuthorizeParentMenuModel();
                    apModel.menu_group_id = Request.Params["id"];
                    apModel.menu_id = arrResultChk[i];
                    apModel.str_is_active = "false";
                    lsPost.Add(apModel);
                }
            }

            Models.Authorize au = new Models.Authorize();

            var Result = au.updateUserGroupMenu(lsPost);
            if (Result.ResultCode == 0)
            {
                return RedirectToAction("Authorize");
            }
            else
                return RedirectToAction("Authorize_Edit", new { id = strGoup_menu_id, msg = Result.MessageText });//Return Error 
        }


        //[HttpPost]
        //public ActionResult Authorize_Update(FormCollection collection)
        //{
        //    try
        //    {
        //        string[] test = collection.GetValues("chkAuth");
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //    return View();
        //}


        //public JsonResult UserGroup()
        //{
        //    var vm = clients.AsQueryable();
        //    var ajaxGridFactory = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
        //    var grid = ajaxGridFactory.CreateAjaxGrid(vm, 1, false);
        //}

        //public JsonResult PersonsPaged(int page)
        //{
        //    var vm = clients.AsQueryable();
        //    var ajaxGridFactory = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
        //    var grid = ajaxGridFactory.CreateAjaxGrid(vm, page, true);
        //}
    }
}

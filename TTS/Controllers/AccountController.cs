﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TTS.Controllers
{
    public class PropLogin
    {
        public int isAuthen { get; set; }
        public string ResultMessage { get; set; }
    }

    public class AccountController : Controller
    {
        //
        // GET: /Acount/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginUser(Models.UserAuthModel user)
        {
            PropLogin LoginResult = new PropLogin();
            if (ModelState.IsValid)
            {
                var resultAuthen = user.AuthenUser(user.UserName, user.Password);
                if (resultAuthen.ResultCode == 0)
                {
                    FormsAuthentication.SetAuthCookie(user.UserName, user.RememberMe);
                    LoginResult.isAuthen = 1;
                    LoginResult.ResultMessage = "";
                    //return RedirectToAction("Index", "Home");
                    //return LoginResult;
                    Session["ses_sys_data"] = resultAuthen.Data;
                    DataSet ds = (DataSet)resultAuthen.Data;
                    Session["ses_sys_db_connection"] = ds.Tables["Application"].Rows[0]["db_connectionstring"].ToString();
                    Session["ses_sys_version"] = ds.Tables["Application"].Rows[0]["app_version"].ToString();
                    Session["ses_user_id"] = user.UserName;
                    //return RedirectToAction("Index", "Home");
                }
                else
                {
                    //ModelState.AddModelError("", "Login data is incorrect!");
                    LoginResult.isAuthen = 0;
                    LoginResult.ResultMessage = resultAuthen.MessageText.ToString();
                    Session["ses_sys_data"] = null;
                }
            }

            //return View(user);
            return Json(LoginResult);
        }

        [HttpGet]
        public ActionResult LogOff()
        {
            Models.UserAuthModel ua = new Models.UserAuthModel();
            ua.LogoutUser();

            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult DestroySession()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}

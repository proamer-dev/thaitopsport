﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TTS.Controllers
{
    public class SharedController : Controller
    {
        //
        // GET: /Shared/

        public ActionResult Index()
        {
            return View();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // code involving this.Session // edited to simplify
            base.OnActionExecuting(filterContext); // re-added in edit
        }

        public ActionResult _Layout()
        {
            if (Session != null && Session["ses_sys_db_connection"] != null)
            {
                 return View();
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "Home");
            }

            //if (HttpContext.Current.Session["ses_sys_db_connection"].ToString() != null)
            //{
            //    if (Request.IsAuthenticated)
            //    {
            //        FormsAuthentication.SignOut();
            //    }
            //}
           
        }
    }
}

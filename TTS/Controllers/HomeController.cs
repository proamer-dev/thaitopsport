﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TTS.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            if (Session["ses_sys_data"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("DestroySession", "Account");
            }
        }

    }
}

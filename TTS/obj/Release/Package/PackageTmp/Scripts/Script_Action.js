﻿
    $(document).ready(function () {
        $(".link_delete").click(function (event_link) {
            var getID = $(this).attr("id");
            bootbox.dialog({
                size: "small",
                message: "ยืนยันการลบข้อมูลหรือไม่?",
                title: "Confirm!",
                buttons: {
                    success: {
                        label: "ยืนยัน",
                        className: "btn-success",
                        callback: function (result) {
                            window.location = $("#" + getID).attr('href');
                        }
                    },
                    danger: {
                        label: "ยกเลิก",
                        className: "btn-danger"
                    }
                }
            });
            event_link.preventDefault();
        });
    });

